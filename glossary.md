# Ventilator Measurements

| Term  | Unit |
| --- | --- |
| Peak Pressure  | cm H2O |
| Plateau Pressure (Pplat)  | cm H2O |
| Mean Airway Pressure  | cm H2O |
| PEEPTOT (total positive end-expiratory pressure)  | cm H2O |
| FiO2  | % |
| I:E ratio (inspiratory to expiratory)  | Ratio |
| Term  | Unit |
| VTe (Tidal volume exhaled)  | mL |
| VE (minute ventilation)  | mL |
| DP (driving pressure)  | cm H2O |
| PEEPi (intrinsic PEEP or auto-PEEP)  | cm H2O |
| ETime (expiratory time)  | seconds |
| Resistance  | cm H2O/L/sec |

# Ventilator Settings

| Term  | Unit |
| --- | --- |
| Pressure Control  | cm H2O |
| Pressure Support  | cm H2O |
| Frequency (rate)  | Breaths/min |
| Inspiratory Pause  | seconds |
| FiO2  | % |
| Trigger Sensitivity  | L/min or cm H2O |
| Term  | Unit |
| VT (tidal volume)  | mL |
| Flow  | L/min |
| iTime (inspiratory time)  | seconds |
| Inspiratory Cycle  | % |
| Rise time  | seconds |


Source: https://courses.edx.org/courses/course-v1:HarvardX+COV19x+1T2020/
