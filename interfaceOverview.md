# Module Structure

> Status: first draft

![Modules and interfaces](assets/images/uml/interfaces.png)

**TBD**
"Extended user interface": 
* user training, documentation
* medical controlling
* development (feedback, prototyping)
* scientific community (data quality, anonymization)



```uml
@startuml 

title "Emergency RAVE Modules and Interfaces V 0.1"
skinparam componentStyle uml2

component "Device Controller" as dc
component "Device UI" as ui
component "Device Simulator" as sim
component "Device" as dv {
  component "Pressure sensor 1" as ps1
  component "Valve 1" as va1
  component "Flow sensor" as fs
  component "Pressure generator" as pg
  component "..." as etc
}
actor "Medic" as md 
actor "Patient" as pt
interface "Controller API" as capi
interface "GPIO" as gpio
interface "GUI" as gui
interface "Physical connection" as phys

dc -down- capi
dc -down- gpio
gpio -down- ps1
gpio -down- va1
gpio -down- fs
gpio -down- etc
gui -up- ui
md -up- gui
dv -down- phys
phys -down- pt
sim -left- gpio 
ui -up- capi


@enduml
```
