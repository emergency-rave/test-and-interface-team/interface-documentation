[![Gitter](https://badges.gitter.im/EmergencyRave/interface-team.svg)](https://gitter.im/EmergencyRave/interface-team?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

```uml
@startuml 

skinparam componentStyle uml2

component "Device Controller" as dc
component "Device UI" as ui
interface "Controller API" as capi

dc -left- capi
ui -right- capi


@enduml
```

The Controller API defines a REST interface in OpenAPI format.

# Specification
See https://gitlab.com/emergency-rave/controllers-and-sensors/device-api/-/blob/master/spec/deviceApi.json

# Java Client
See https://gitlab.com/emergency-rave/controllers-and-sensors/device-api-java-client

# Documentation

## sensorReadings
|Field|Type|Description|Example|Properties|
|---|---|---|---|---|
|timestamp|integer($int32)||||
|pressureMixChamber|integer($int32)||||
|airFlow|number($double)||||
|pressureLung|integer($int32)||||
|valveStatusIntervalSec|number($double)||||
|compressorValveOpenRatio|number($double)||||
|oxygenValveOpenRatio|number($double)||||
|inspirationValveOpenRatio|number($double)||||
|expirationValveOpenRatio|number($double)||||

## runtimeParameters
|Field|Type|Description|Example|Properties|
|---|---|---|---|---|
|oxygenPercent|integer($int32)|percentage of oxygen allowed to flow inside the tank, when oxygen and air are mixed. Allowed values: 0-100|||
|respirationsPerMinute|integer($int32)|frequency at which the patient breaths, in breathing cycles per minute. Allowed values: 6-17|||
|tankMaxPressure|integer($int32)|Upper end of the pressure range in the tank. If the pressure is higher than the limit, the compressor turns off. Allowed values: 0-14500|||
|tankMinPressure|integer($int32)|Lower end of pressure range in the tank. If the pressure is lower than the limit, the compressor turns on to refill the tank. Allowed values: 0-14500|||
|lungsMinPressure|integer($int32)|Lower end of lung pressure range. If the pressure is lower than the limit, the inspiration valve turns on after each period passes. Allowed values: 0-1600|||
|lungsMaxPressure|integer($int32)|Upper end of lung pressure range. If the pressure is higher than the limit, the inspiration valve turns off after each period passes. Allowed values: 0-1600|||
|lungsCriticalPressure|integer($int32)|Warning level for pressure in the lungs. Allowed values: 0-1600|||
|operationsMode|string|Operational Mode, one of "pressure", "time", "interactive". |||
|inspirationTimePercent|integer($int32)|percentage of time that inspiration takes relative to the length of the  breathing period. Range: 25-75|||
|expirationTimePercent|integer($int32)|percentage of time that expiration takes relative to the length of the  breathing period. Allowed range: 25-75|||
|pressureUnit|string|One of "mpsi", "pa", "cmh2o", "mmhg", "mbar"|||
|temperatureUnit|string|One of "celsius", "farenheit"|||

## deviceHealth
|Field|Type|Description|Example|Properties|
|---|---|---|---|---|
|status|string||||
|issues|array|||items: OrderedDict([('type', 'object'), ('properties', OrderedDict([('timestamp', OrderedDict([('format', 'int32'), ('type', 'integer')])), ('message', OrderedDict([('type', 'string')])), ('count', OrderedDict([('format', 'int32'), ('type', 'integer')])), ('severety', OrderedDict([('type', 'string')]))]))])|
## calibrationResult
|Field|Type|Description|Example|Properties|
|---|---|---|---|---|
|venturi-system-const|integer($int32)||||
|compressor-mbar-max|integer($int32)||||
|flow-ml-sec-max|integer($int32)||||
## treatmentParameters
|Field|Type|Description|Example|Properties|
|---|---|---|---|---|
|pressure|number($float)|required lung pressure in mbar|||
|frequency|number($float)|breath cycles per second|||
|flow|number($float)|moved gas volume per breath cycle in ml|||
|lung-tidal-vol|number($float)|tidal volume (TV) of lung in ml|||
|lung-inspiratory-reserve-vol|number($float)|inspiratory reserve volume (IRV) of lung in ml|||
|lung-expiratory-reserve-vol|number($float)|expiratory reserve volume (ERV) of lung in ml|||
|lung-residual-vol|number($float)|residual volume (RV) of lung in ml|||
